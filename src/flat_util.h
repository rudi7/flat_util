namespace FlatUtil
{
    void write_buffer(uint8_t *buff_ptr, int, const char*);
    char* read_buffer(const char*);
    flatbuffers::Offset<nodeflux::any::Any> pack_from(flatbuffers::FlatBufferBuilder &builder);

    template <typename T>
    const T *unpack_to(nodeflux::any::Any const *message)
    {
        return flatbuffers::GetRoot<T>(message->value()->data());
    }
}
