#include <fstream>
#include <flatbuffers/flatbuffers.h>

#include "any_generated.h"
#include "flat_util.h"


namespace FlatUtil
{
    flatbuffers::Offset<nodeflux::any::Any> pack_from(flatbuffers::FlatBufferBuilder &builder)
    {    
        auto any_value = builder.CreateVector(builder.GetBufferPointer(), builder.GetSize());
        auto any_type_url = builder.CreateString("types.nodeflux.io/Frame.Scheme.Data");
        auto any = nodeflux::any::CreateAny(
            builder, any_type_url, any_value
        );

        builder.Finish(any);

        return any;
    }

    void write_buffer(uint8_t *buff_ptr, int size, const char* filename)
    {
        FILE *file;

        file = fopen(filename, "wb");
        fwrite(&buff_ptr[0], size, 1, file);
        fclose(file);
    }

    char* read_buffer(const char* filename)
    {
        std::ifstream infile;
        infile.open(filename, std::ios::binary | std::ios::in);

        infile.seekg(0, std::ios::end);
        int length = infile.tellg();
        infile.seekg(0, std::ios::beg);

        char *data = new char[length];

        infile.read(*(&data), length);
        infile.close();

        return data;
    }
}
