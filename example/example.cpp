#include <assert.h>
#include <iostream>

#include <flatbuffers/flatbuffers.h>
#include "data_generated.h"
#include "file_generated.h"
#include "frame_generated.h"

#include "../src/flat_util.h"

using namespace std;

int main()
{
    // Data Struct Declaration
    /*
    flatbuffers::FlatBufferBuilder builder(2048);
    auto data_id = 666;
    auto data_name = builder.CreateString("Rudi Muliawan");
    auto data = data::CreateData(
        builder, data_id, data_name
    );
    builder.Finish(data);
    */

    flatbuffers::FlatBufferBuilder builder(2048);
    int file_id = 777;
    auto file_name = builder.CreateString("nodeflux");
    auto file_path = builder.CreateString("/home/nodeflux/test");
    auto file = file::CreateFile(
        builder, file_id, file_name, file_path
    );
    builder.Finish(file);

    // Store File Struct's buffer to Any->value
    auto any = FlatUtil::pack_from(builder);

    // Frame Struct Declaration
    int frame_size = 108;
    int frame_width = 92;
    int frame_height = 12;
    int frame_pixel = 22; 
    auto frame = frame::CreateFrame(
        builder, frame_size, frame_width, frame_height, frame_pixel,
        any
    );
    builder.Finish(frame);

    uint8_t *buff = builder.GetBufferPointer();

    auto loaded_frame = flatbuffers::GetRoot<frame::Frame>(buff);
    cout << loaded_frame->size() << endl;
    cout << loaded_frame->width() << endl;
    cout << loaded_frame->height() << endl;
    cout << loaded_frame->pixel() << endl;

    // Convert frame->data(any) to Data
    // auto loaded_data = FlatUtil::unpack_to<data::Data>(loaded_frame->data());
    // cout << loaded_data->id() << endl;
    // cout << loaded_data->name()->str() << endl;

    auto loaded_file = FlatUtil::unpack_to<file::File>(loaded_frame->data());
    cout << loaded_file->id() << endl;
    cout << loaded_file->name()->str() << endl;
    cout << loaded_file->path()->str() << endl;

    return 0;
}
